package puzzle;

import java.util.Scanner;

import puzzle.SwappDash.Puzzle;

public class Puzzle01 {
	static int row = 0;
	static int col = 0;
   
	public static void main(String[] args) {
		
		
		String str;
		Scanner sc = new Scanner(System.in);
		
		char[][] puz = { { 'T', 'R', 'G', 'S', 'J' }, { 'X', 'D', 'O', 'K', 'I' }, { 'M', '_', 'V', 'L', 'N' },
				{ 'W', 'P', 'A', 'B', 'E' }, { 'U', 'Q', 'H', 'C', 'F' } };

		for (int i = 0; i < puz.length; i++) {
			for (int j = 0; j < puz.length; j++) {
				System.out.print((puz[i][j]));
			}
			System.out.println();
		}
		

		findSpace(puz);
		str = sc.next();
		doMoves(puz, str);

		if (row == -1 || col == -1) {
			System.out.println("no configaration");
		}

	}

	public static void findSpace(char[][] puz) {
		String str = "";
		for (int i = 0; i < puz.length; i++) {
			for (int j = 0; j < puz.length; j++) {
				if (puz[i][j] == '_') {
					row = i;
					col = j;
				}
			}

		}
	}

	public static void swapSpace(char[][] puz, int row2, int col2) {
		char tmp = puz[row][col];
		puz[row][col] = puz[row2][col2];
		puz[row2][col2] = tmp;
	}

	public static boolean isValidMove(char[][] puz, char c) {
		switch (c) {
		case 'A':
			if (row == 0)
				return false;
			swapSpace(puz, row - 1, col);
			row--;
			print(puz);
			System.out.println();
			return true;
		case 'B':
			if (row == 4)
				return false;
			swapSpace(puz, row + 1, col);
			row++;
			print(puz);
			System.out.println();
			return true;
		case 'L':
			if (col == 0)
				return false;
			swapSpace(puz, row, col - 1);
			col--;
			print(puz);
			System.out.println();
			return true;
		case 'R':
			if (col == 4)
				return false;
			swapSpace(puz, row, col + 1);
			col++;
			print(puz);
			System.out.println();
			return true;
		case '0':
			return false;
		     
		default:
			System.out.println("Bad move: '" + c + "'");
			return false;
		}
	}

	public static void print(char[][] puz) {
		for (int i = 0; i < puz.length; i++) {
			for (int j = 0; j < puz.length; j++) {
				System.out.print((puz[i][j]));
			}
			System.out.println();
		}
	}

	public static void doMoves(char[][] puz, String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c == '0')
				return ;
			if (!isValidMove(puz, c)) {
				row = -1;
				col = -1;
				return;
			}
		}
	}

}
